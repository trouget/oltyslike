@extends('template/templateAdmin')
@section('content')
<table class="table table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 2px">#</th>
            <th style="width: 10px">Titre</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tab_news as $uneNews)
        <tr>
            <td class="col-md-1">
                {{ $uneNews["id"] }}
            </td>

            <td class="col-md-2">
                {{ $uneNews["titre"] }}
            </td>
            <td class="col-md-1">
                {{ $uneNews["contenu"] }}
            </td>
            <td class="col-md-2">
            {{ Form::open(['route'=>['news.destroy',$uneNews->id], 'method' => 'delete']) }}
            {{Form::submit('Supprimez',["class"=>"btn btn-block btn-danger"])}}
            {{ Form::close() }}
            </td>
            <td class ="col-md-2">
            {{ Form::open(['route'=>['news.edit',$uneNews->id],"method"=>"get"]) }}
            {{Form::submit('Editer',["class"=>"btn btn-primary btn-block"])}}
            {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop