@extends('template/templateAdmin')
@section('content')
<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => ["habitants.update", $habitant["id"]], 'method' => 'put']) !!}
            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Nom de l'habitant:  </label>
                    <input class="form-control" name="nom" value={{$habitant["nom"]}}>
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Prénom de l'habitant:  </label>
                    <input class="form-control" name="prenom" value={{$habitant["prenom"]}}>
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Email de l'habitant:  </label>
                    <input class="form-control" name="email" value={{$habitant["email"]}}>
                </div>
            </div>

            
    <div class="form-group" style="width:200px;">
     <label for="role" class="control-label">Animal possédé</label>
        <select class="form-control" id="animalCompagnie" name="animalCompagnie">
        <option value="1">Aucun</option>
        <option value="2">Chien</option>
        <option value="3">Chat</option>
        <option value="4">Les deux</option>
        <option value="5">Autres</option>
     </select>
    </div>

            <!-- /.box-header -->
            <div class="box-body pad">

                <div class="form-group">
                    {{ Form::textarea('editor', $habitant["description"],['id'=>'editor','class'=>'form-control']) }}
                </div>

            </div>

            <div class="form-group">
                <a href="" class="popup_selector" data-inputid="feature_image"><label for="feature_image">Selectionner Image</label></a>                   
                <input type="text" id="feature_image" name="image" value={{$habitant['image']}}>
            </div>

            <button type="submit" class="btn btn-success btn-lg btn-block">Modifier l'Habitant</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>
@stop