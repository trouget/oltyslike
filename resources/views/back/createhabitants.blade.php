@extends('template/templateAdmin')
@section('content')
<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => "habitants.store", 'method' => 'post', 'files' => true]) !!}
            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Nom de l'habitant:  </label>
                    <input class="form-control" placeholder="Le nom" name="nom">
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Prénom de l'habitant:  </label>
                    <input class="form-control" placeholder="Le prenom" name="prenom">
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Email de l'habitant:  </label>
                    <input class="form-control" placeholder="email" name="email">
                </div>
            </div>

            
    <div class="form-group" style="width:200px;">
     <label for="role" class="control-label">Animal possédé</label>
        <select class="form-control" id="animalCompagnie" name="animalCompagnie">
        <option value="1">Aucun</option>
        <option value="2">Chien</option>
        <option value="3">Chat</option>
        <option value="4">Les deux</option>
        <option value="5">Autres</option>
     </select>
    </div>

            <!-- /.box-header -->
            <div class="box-body pad">

                <div class="form-group">
                    {{ Form::textarea('editor', '',['id'=>'editor','class'=>'form-control','placeholder'=>'Rentrer la description des animaux ici']) }}
                </div>

            </div>

            <div class="form-group">
                <a href="" class="popup_selector" data-inputid="feature_image"><label for="feature_image">Selectionner Image</label></a>                   
                <input type="text" id="feature_image" name="image" value="">
            </div>

            <button type="submit" class="btn btn-success btn-lg btn-block">Créer</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>

<!--script elfinder -->
<script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.min.js"></script>
@stop