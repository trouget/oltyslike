@extends('template/templateAdmin')

@section('content')
<table class="table table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 2px">#</th>
            <th style="width: 10px">Titre</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($LesHabitants as $unHabitant)
        <tr>
            <td class="col-md-1">
                {{ $unHabitant["id"] }}
            </td>

            <td class="col-md-1">
                {{ $unHabitant["nom"] }}
            </td>
            <td class="col-md-2">
                {{ $unHabitant["prenom"] }}
            </td>
            <td class="col-md-4">
                {{ $unHabitant["email"] }}
            </td>
            <td class="col-md-4">
                {{ $unHabitant["animalCompagnie"] }}
            </td>
            <td class="col-md-5">
                {{ $unHabitant["description"] }}
            </td>
            <td class="col-md-1">
            {{ Form::open(['route'=>['habitants.destroy',$unHabitant->id], 'method' => 'delete']) }}
            {{Form::submit('Supprimez',["class"=>"btn btn-block btn-danger"])}}
            {{ Form::close() }}
            </td>
            <td class ="col-md-1">
            {{ Form::open(['route'=>['habitants.edit',$unHabitant->id],"method"=>"get"]) }}
            {{Form::submit('Editer',["class"=>"btn btn-warning btn-block"])}}
            {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop