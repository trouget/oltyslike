@extends('template/template')
@section('title')
News du site
@stop
@section('content')

<!--
<table class="table table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 2px">Date</th>
            <th style="width: 10px">Titre</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($lesNews as $uneNews)
        <tr>
            <td class="col-md-1">
                {{ $uneNews["created_at"] }}
            </td>

            <td class="col-md-2">
                {{ $uneNews["titre"] }}
            </td>
            
            <td class="col-md-1">
                {{ $uneNews["contenu"] }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table> -->


<!--                                    -->
    <div class="service_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb-70">
                        <span class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s" >Les actualités</span>
                        <h3 class="wow fadeInUp" data-wow-duration="1.2s" data-wow-delay=".2s">Retrouvés ci dessous les actualités du village!</h3>
                    </div>
                </div>
            </div>

            @foreach ($lesNews as $uneNews)
                <div class="col-xl-4 col-md-4">
                    <div class="single_service text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="icon">
                            <img src="img/svg_icon/2.svg" alt="">
                        </div>
                        <h3>{{ $uneNews["titre"] }}</h3>
                        <p>{{ $uneNews["created_at"] }}</p>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
 <!--/ service_area  -->

 @stop