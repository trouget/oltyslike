@extends('template/template')
@section('title')
Menu principal
@stop

@section('content')


    <!-- service_area  -->
    <div class="service_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb-70">
                        <span class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s" >Services</span>
                        <h3 class="wow fadeInUp" data-wow-duration="1.2s" data-wow-delay=".2s">With more than 20 years of 
                            experience we can deliver the 
                            best product design.</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-4">
                    <div class="single_service text-center wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".4s">
                        <div class="icon">
                            <img src="img/svg_icon/1.svg" alt="">
                        </div>
                        <h3>Graphic design</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="single_service text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="icon">
                            <img src="img/svg_icon/2.svg" alt="">
                        </div>
                        <h3>Web design</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="single_service text-center wow fadeInRight" data-wow-duration="1.2s" data-wow-delay=".4s">
                        <div class="icon">
                            <img src="img/svg_icon/3.svg" alt="">
                        </div>
                        <h3>Mobile app</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!--/ service_area  -->

 <div data-scroll-index="0" class="get_in_tauch_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title text-center mb-90">
                    <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Contactez nous!</h3>
                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">Vous pouvez nous envoyer un message via ce formulaire et nous tâcherons d'y répondre dans les plus brefs délais.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="touch_form">
                    <form action="#">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                                    <input type="text" placeholder="Votre nom" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
                                    <input type="email" placeholder="Email" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                                    <input type="email" placeholder="Sujet" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
                                   <textarea name="" id="" cols="30" placeholder="Message" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="submit_btn wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                                    <button class="boxed-btn3" type="submit">Envoyer le message</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- footer start -->
<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 col-md-3">
                    <div class="footer_logo wow fadeInRight" data-wow-duration="1s" data-wow-delay=".3s">
                        <a href="index.html">
                            <img src="img/logo.png" alt="">
                        </a>
                    </div>
                </div>                
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ footer end  -->
@stop