<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Habitants;

class habitantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lesHabitants = Habitants::all();
        return view('back.modifyhabitants')->with("LesHabitants", $lesHabitants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.createhabitants');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->session()->flash('success', 'habitant' . $request->get('nom') . ' à été ajoutée !');

        $Habitant = new Habitants();

        $Habitant->nom = $request->get('nom');
        $Habitant->prenom = $request->get('prenom');
        $Habitant->email = $request->get('email');
        $Habitant->animalCompagnie = $request->get('animalCompagnie');
        $Habitant->description = $request->get('editor');
        $Habitant->image = $request->get('image');

        $Habitant->save();
        return redirect()->route("back.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $habitant = Habitants::find($id);
        return view('back.edithabitants')->with("habitant", $habitant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Habitant = Habitants::find($id);
        
        $Habitant->nom = $request->get('nom');
        $Habitant->prenom = $request->get('prenom');
        $Habitant->email = $request->get('email');
        $Habitant->animalCompagnie = $request->get('animalCompagnie');
        $Habitant->description = $request->get('editor');

        $Habitant->save();
        return redirect()->route("habitants.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Habitant = Habitants::find($id);
        Habitants::destroy($id);
        return redirect()->route("habitants.index");
    }
}
