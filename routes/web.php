<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('front', 'frontController');
Route::resource('news', 'newsController');
Route::resource('habitants', 'habitantsController');
Route::resource('back', 'adminController');

Route::get('/Frontnews','frontController@FrontNews')->name("FrontNews");
Route::get('/FrontContact','frontController@FrontContact')->name("FrontContact");

Route::get('/', function () {
    return view('accueil');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
